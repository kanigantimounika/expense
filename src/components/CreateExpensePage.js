import React from 'react';
import ExpenseForm from './ExpenseForm'
import {addExpense} from '../actions/expense'
import {connect} from 'react-redux'


export class CreateExpensePage extends React.Component{

    onSubmit = (expense) => {
        this.props.startAddExpense(expense);
        this.props.history.push('/');
      };
      render() {
        return (
          <div>
            <div className="page-header">
              <div className="content-container">
                <h1 className="page-header__title">Add Expense</h1>
              </div>
            </div>
            <div className="content-container">
              <ExpenseForm
                onSubmit={this.onSubmit}
              />
            </div>
          </div>
        );
      }
    }
    

    const mapDispatchToProps = (dispatch) => ({
        startAddExpense: (expense) => dispatch(addExpense(expense))
      });
      
      export default connect(undefined, mapDispatchToProps)(CreateExpensePage);