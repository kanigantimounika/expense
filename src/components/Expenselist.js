import React from 'react';
import {connect}  from 'react-redux'
import ExpenseListItem from './ExpenseListItems';
import ExpenseListItems from './ExpenseListItems'
import SelectExpense from '../selectors/expense'
export const Expsneslist=(props)=>(
    <div>
    
   {
       props.expenses.length===0?(
           <p>No expenses</p>
       ):
       (props.expenses.map((expense)=>{
         return <ExpenseListItem key={expense.id} {...expense} />
}
)
)
}

    
    </div>
)

const ConnectExpenseList=(state)=>{
    return {
        expenses:SelectExpense(state.expenses,state.filters)
       
    }
}
export default connect(ConnectExpenseList)(Expsneslist)