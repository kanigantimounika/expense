import moment from "moment"
import {setEndDate,setStartDate,setTextFilter,sortByDate,sortByAmount} from "../../actions/filter"



test("end",()=>{

    const action=setEndDate(moment(0))
    expect(action).toEqual({
    type: 'SET_END_DATE',
    endDate:moment(0)
    })
})
test("start",()=>{

    const action=setStartDate(moment(0))
    expect(action).toEqual({
        type: 'SET_START_DATE',
        startDate:moment(0)
    })
})



test("filter text",()=>{
    const text="hello"
    const action=setTextFilter(text)
    expect(action).toEqual({
        type: 'SET_TEXT_FILTER',
        text
    })
})
test("amount ",()=>{
 
    const action=sortByAmount()
    expect(action).toEqual({
        type: 'SORT_BY_AMOUNT',
        
    })
})
test("date ",()=>{
 
    const action=sortByDate()
    expect(action).toEqual({
        type: 'SORT_BY_DATE',
        
    })
})