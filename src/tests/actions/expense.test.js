import {addExpense,editExpense,removeExpense} from "../../actions/expense"

test("remove",()=>{
    const action=removeExpense({id:'1234'})
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id:'1234'
    })
})
test("edit",()=>{
    const action=editExpense('1234aa',{note:'hello'});
    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id:'1234aa',
        updates:{
            note:'hello'
        }
        
    })
   
})

test("create a expense",()=>{
    const expensedata={
        description: 'abc',
        note : 'teh',
        amount : 2120,
        createdAt : 10121
      }
      const action=addExpense(expensedata)
      expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: {
            ...expensedata,
          id: expect.any(String)
         
        }
      })
})

