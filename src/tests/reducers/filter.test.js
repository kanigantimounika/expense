
import moment from 'moment';
import filterreducer from "../../reducers/filters"

test("check default",()=>{
    const state=filterreducer(undefined,{type:'@@INIT'})
    expect(state).toEqual({
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
    })
})

test("amount",()=>{
    const state=filterreducer(undefined,{type:'SORT_BY_AMOUNT'})
    expect(state.sortBy).toBe('amount')
})
test("date",()=>{
    const action={
    text: '',
    sortBy: 'amount',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
    }
    const state=filterreducer(action,{type:'SORT_BY_DATE'})
    expect(state.sortBy).toBe('date')
})
test("date",()=>{
    const startDate=moment()
    const action={
        type:'SET_START_DATE',
        startDate
    }
  
    const state=filterreducer(undefined,action)
    expect(state.startDate).toEqual(startDate)
})
test("enddate",()=>{
    const endDate=moment()
    const action={
        type:'SET_END_DATE',
        endDate
    }
  
    const state=filterreducer(undefined,action)
    expect(state.endDate).toEqual(endDate)
})