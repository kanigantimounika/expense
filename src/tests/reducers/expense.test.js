
import moment from "moment"
import expensereducer from "../../reducers/expense"

const expenses=[{
    id:'1',
    description: 'Rent',
    note : '',
    amount : 2120,
    createdAt : moment(0)
},
{
    id:'2',
    description: 'water',
    note : '',
    amount : 123,
    createdAt : moment(0).subtract(4,'days').valueOf()
},
{
    id:'3',
    description: 'gas',
    note : '',
    amount : 1000,
    createdAt : moment(0).add(4,'days').valueOf()
}]


test("default",()=>{
    const state=expensereducer(undefined,{type:'@@INIT'})
    expect(state).toEqual([])

    
})

test("remove ",()=>{
    const action={
        type:'REMOVE_EXPENSE',
        id:expenses[1].id
    }
    const state=expensereducer(expenses,action)
    expect(state).toEqual([expenses[0],expenses[2]])

    
})
test("remove ",()=>{
    const action={
        type:'REMOVE_EXPENSE',
        id:6
    }
    const state=expensereducer(expenses,action)
    expect(state).toEqual(expenses)

    
})
test("edit ",()=>{
    const amount=12345
    const action={
        type:'EDIT_EXPENSE',
        id:expenses[0].id,
        updates:{
            amount
        }
    }
    const state=expensereducer(expenses,action)
    expect(state[0].amount).toBe(amount)

    
})
test("edit ",()=>{
    const amount=12345
    const action={
        type:'EDIT_EXPENSE',
        id:-1,
        updates:{
            amount
        }
    }
    const state=expensereducer(expenses,action)
    expect(state).toEqual(expenses)

    
})

test("add ",()=>{
    const expense={
        id:'10',
        description: 'aaa',
        note : '',
        amount : 12345,
        createdAt : moment(0)
    }
    const action={
        type:'ADD_EXPENSE',
        expense
    }
    const state=expensereducer(expenses,action)
    expect(state).toEqual([...expenses,expense])

    
})