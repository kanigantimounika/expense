import React from 'react';
import {shallow} from 'enzyme';
import ExpenseForm from '../../components/ExpenseForm'
import expenses from '../fixtures/expense'
import moment from'moment'
test("render expense form with default",()=>{
    const wrapper=shallow(<ExpenseForm/>)
    expect(wrapper).toMatchSnapshot();
})

test("render expense form with data",()=>{
    const wrapper=shallow(<ExpenseForm expenses={expenses[1]}/>)
    expect(wrapper).toMatchSnapshot();
})

test("render form",()=>{
    const wrapper=shallow(<ExpenseForm/>)
    expect(wrapper).toMatchSnapshot();
    wrapper.find('form').simulate("submit",{
        preventDefault:()=>{}
    })
    expect(wrapper.state("error").length).toBeGreaterThan(0)
    expect(wrapper).toMatchSnapshot();
})
test("description",()=>{
    const value='Description'
    const wrapper=shallow(<ExpenseForm/>)
    wrapper.find("input").at(0).simulate('change',{
        target:{value}
    })
    expect(wrapper.state('description')).toBe(value)
})

test("description",()=>{
    const value='text'
    const wrapper=shallow(<ExpenseForm/>)
    wrapper.find("textarea").at(0).simulate('change',{
        target:{value}
    })
    expect(wrapper.state('note')).toBe(value)
})

test("amount crct",()=>{
    const value='12.11'
    const wrapper=shallow(<ExpenseForm/>)
    wrapper.find("input").at(1).simulate('change',{
        target:{value}
    })
    expect(wrapper.state('amount')).toBe(value)
})

test("amount crct",()=>{
    const value='12.111'
    const wrapper=shallow(<ExpenseForm/>)
    wrapper.find("input").at(1).simulate('change',{
        target:{value}
    })
    expect(wrapper.state('amount')).toBe('')
})

test("submitting form ",()=>{
    const submitspi=jest.fn();
    const wrapper=shallow(<ExpenseForm expense={expenses[0]} onSubmit={submitspi}/>)
    wrapper.find('form').simulate('submit',{
        preventDefault:()=>{}
    })
    expect(wrapper.state('error')).toBe('')
    expect(submitspi).toHaveBeenLastCalledWith({
        description: expenses[0].description,
        amount: expenses[0].amount,
        note: expenses[0].note,
        createdAt: expenses[0].createdAt
    }
    )
})

test("should test new start date",()=>{
    const datepass=new moment();
    const wrapper=shallow(<ExpenseForm />)
    wrapper.find('SingleDatePicker').prop('onDateChange')(datepass)
    expect(wrapper.state('createdAt')).toEqual(datepass)

    

})
test('should set calendar focus on change', () => {
    const focused = true;
    const wrapper = shallow(<ExpenseForm />);
    wrapper.find('SingleDatePicker').prop('onFocusChange')({ focused });
    expect(wrapper.state('calendarFocused')).toBe(focused);
  });