import React from 'react';
import {shallow} from 'enzyme';
import {Expsneslist} from '../../components/ExpenseList'
import expenses from '../fixtures/expense'


test("should render expense with exppenses lis",()=>{
    const wrapper=shallow(<Expsneslist expenses={expenses}/>)
    expect(wrapper).toMatchSnapshot()
})

test("should render expense with empty exppenses lis",()=>{
    const wrapper=shallow(<Expsneslist expenses={[]}/>)
    expect(wrapper).toMatchSnapshot()
})