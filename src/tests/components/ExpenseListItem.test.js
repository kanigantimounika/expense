import React from 'react';
import {shallow} from 'enzyme';
import ExpenseListItem from '../../components/ExpenseListItems'
import expenses from '../fixtures/expense'

test("should render expense with exppenses lis",()=>{
    const wrapper=shallow(<ExpenseListItem {...expenses[0]}/>)
    expect(wrapper).toMatchSnapshot()
})
