import moment from "moment"
import selector from "../../selectors/expense"


const expenses=[{
    id:'1',
    description: 'Rent',
    note : '',
    amount : 2120,
    createdAt : moment(0)
},
{
    id:'2',
    description: 'water',
    note : '',
    amount : 123,
    createdAt : moment(0).subtract(4,'days').valueOf()
},
{
    id:'3',
    description: 'gas',
    note : '',
    amount : 1000,
    createdAt : moment(0).add(4,'days').valueOf()
}]

test("amount",()=>{
    const filters={
        text:'',
        sortBy:'amount',
        startDate:undefined,
        endDate:undefined
    }
    const action=selector(expenses,filters)
    expect(action).toEqual([
        expenses[0],expenses[2],expenses[1]]
    )
})
test("amount",()=>{
    const filters={
        text:'',
        sortBy:'date',
        startDate:undefined,
        endDate:undefined
    }
    const action=selector(expenses,filters)
    expect(action).toEqual([
        expenses[2],expenses[0],expenses[1]]
    )
})

test("text",()=>{
    const filters={
        text:'e',
        sortBy:'date',
        startDate:undefined,
        endDate:undefined
    }
    const action=selector(expenses,filters)
    expect(action).toEqual([
        expenses[0],expenses[1]]
    )
})

test("startdate",()=>{
    const filters={
        text:'',
        sortBy:'date',
        startDate:moment(0),
        endDate:undefined
    }
    const action=selector(expenses,filters)
    expect(action).toEqual([
        expenses[2],expenses[0]]
    )
})
test("enddate",()=>{
    const filters={
        text:'',
        sortBy:'date',
        startDate:0,
        endDate:moment(0)
    }
    const action=selector(expenses,filters)
    expect(action).toEqual([
        expenses[0],expenses[1]]
    )
})