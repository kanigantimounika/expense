import {Route,BrowserRouter,Switch,Link,NavLink} from 'react-router-dom';
import React from 'react';
import Header from '..//components/Header'
import ExpenseDashBoard from '..//components/ExpenseDashBoard'
import CreateExpensePage from '..//components/CreateExpensePage'
import EditExpensePage from '..//components/EditExpensePage'
import HelpPage from '..//components/HelpPage'
import PageNotFound from '..//components/PageNotFound'




const AppRouter=()=>(
    <BrowserRouter>
    <div>
    <Header />
    <Switch>
    <Route path='/' component={ExpenseDashBoard} exact={true}></Route>
    <Route path='/create' component={CreateExpensePage}></Route>
    <Route path='/edit/:id' component={EditExpensePage}></Route>
    <Route path='/help'   component={HelpPage}></Route>
    <Route  component={PageNotFound}></Route>
    </Switch>
    </div>
    </BrowserRouter>


)
export default AppRouter;
